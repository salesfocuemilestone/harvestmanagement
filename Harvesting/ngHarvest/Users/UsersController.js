﻿AdminApp.controller('UsersController', ['$scope', '$rootScope', '$stateParams', '$state', 'LoginService', '$location',
    function ($scope, $rootScope, $stateParams, $state, LoginService, $location) {
        debugger;
        $scope.totalPages = 10;
        $scope.totalItems = 10;
        $scope.page = 1;
        $scope.currentPage = 1;
        var myEl = angular.element(document.querySelector('#lnkUsers'));
        myEl.addClass('dropdownActive');
        $scope.UsersList = [
                    {
                        'FirstName': 'Admin',
                        'LastName': 'User',
                        'Email': 'admin@harvest.com'
                    },
                    	{
                    	    'FirstName': 'Super',
                    	    'LastName': 'User',
                    	    'Email': 'superuser@harvest.com'
                    	},
	                    	{
	                    	    'FirstName': 'Employee',
	                    	    'LastName': 'User',
	                    	    'Email': 'employee@harvest.com'
	                    	},
        ];
        $scope.addUser = function () {
            $state.go('adduser');
        }
    }
]);