AdminApp.controller('ProductsController', ['$scope', '$rootScope', '$stateParams', '$state', 'LoginService', '$location',
    function ($scope, $rootScope, $stateParams, $state, LoginService, $location) {
        var myEl = angular.element(document.querySelector('#lnkMasters'));
        myEl.addClass('dropdownActive');
        $scope.productsList = [
            {
                'Product': 'Product 1'
            },
            {
                'Product': 'Product 2'
            },
            {
                'Product': 'Product 3'
            },
        ];
        $scope.addCustomer = function () {
            $state.go('addcustomer');
        }
    }
]);