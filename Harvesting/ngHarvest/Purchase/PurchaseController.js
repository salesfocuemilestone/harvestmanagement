﻿AdminApp.controller('PurchaseController', ['$scope', '$rootScope', '$stateParams', '$state', 'LoginService', '$location',
    function ($scope, $rootScope, $stateParams, $state, LoginService, $location) {
        var myEl = angular.element(document.querySelector('#lnkStock'));
        myEl.addClass('dropdownActive');
        $scope.customerList = [];
        $scope.productList = [];
        $scope.loadcustomers = function () {
            $scope.customerList = [{ CustomerName: 'Infosys Technologies', CustomerId: 1 }, { CustomerName: 'Cognizant Technologies', CustomerId: 2 }]
        }
        $scope.loadproducts = function () {
            $scope.productList = [{ ProductName: 'Product-1', ProductId: 1 }, { ProductName: 'Product-1', ProductId: 2 }]
        }
    }
]);