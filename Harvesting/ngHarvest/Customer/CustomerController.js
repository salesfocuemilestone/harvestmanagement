﻿AdminApp.controller('CustomerController', ['$scope', '$rootScope', '$stateParams', '$state', 'LoginService', '$location',
    function ($scope, $rootScope, $stateParams, $state, LoginService, $location) {
        var myEl = angular.element(document.querySelector('#lnkMasters'));
        myEl.addClass('dropdownActive');
        $('[data-toggle="tooltip"]').tooltip();
        $scope.companies = [
                    {
                        'name': 'Infosys Technologies',
                        'contact': 970124656,
                        'headoffice': 'Bangalore'
                    },
                    	{
                    	    'name': 'Cognizant Technologies',
                    	    'contact': 970124656,
                    	    'headoffice': 'Hyderabad'
                    	},
	                    	{
	                    	    'name': 'Wipro',
	                    	    'contact': 970124656,
	                    	    'headoffice': 'Bangalore'
	                    	},
		                    	{
		                    	    'name': 'Tata Consultancy Services (TCS)',
		                    	    'contact': 970124656,
		                    	    'headoffice': 'Mumbai'
		                    	},
			                    	{
			                    	    'name': 'HCL Technologies',
			                    	    'contact': 970124656,
			                    	    'headoffice': 'Noida'
			                    	},
        ];
        $scope.addCustomer = function () {
            $state.go('addcustomer');
        }
    }
]);