﻿var AdminApp = angular.module('ngHarvestApp', ['ui.router', 'ngCookies', 'ngSanitize', 'ngAria', 'ngAnimate', 'ngMaterial', 'ngMessages', 'md-steppers']);

var configFunction = function ($stateProvider, $urlRouterProvider, /*$routeProvider, */ $httpProvider, $locationProvider) {

    $locationProvider.hashPrefix('!').html5Mode(true);//{ enabled: true, requireBase: false });
    //$locationProvider.hashPrefix('!').html5Mode({ enabled: true, requireBase: false });

    $stateProvider
        .state('login', {
            url: '/login',
            views: {
                "mainView": {
                    templateUrl: 'ngHarvest/Login/Login.html',
                    controller: 'LoginController'
                }
            }
        }).state('dashboard', {
            url: '/dashboard',
            views: {
                "mainView": {
                    templateUrl: 'ngHarvest/Dashboard/Dashboard.html',
                    controller: 'DashboardController'
                },
                "nestedView@home": {
                    templateUrl: 'ngHarvest/Dashboard/home.html',
                    controller: 'DashboardController'
                }
            }
        }).state('home', {
            url: '/home',
            views: {
                "mainView": {
                    templateUrl: 'ngHarvest/Dashboard/Dashboard.html',
                    controller: 'DashboardController'
                },
                "nestedView@home": {
                    templateUrl: 'ngHarvest/Dashboard/home.html',
                    controller: 'HomeController'
                }
            }
        }).state('customer', {
            url: '/customer',
            views: {
                "mainView": {
                    templateUrl: 'ngHarvest/Dashboard/Dashboard.html',
                    controller: 'DashboardController'
                },
                "nestedView@customer": {
                    templateUrl: 'ngHarvest/Customer/Customer.html',
                    controller: 'CustomerController'
                }
            }
        }).state('addcustomer', {
            url: '/addcustomer',
            views: {
                "mainView": {
                    templateUrl: 'ngHarvest/Dashboard/Dashboard.html',
                    controller: 'DashboardController'
                },
                "nestedView@addcustomer": {
                    templateUrl: 'ngHarvest/Customer/AddCustomer.html',
                    controller: 'AddCustomerController'
                }
            }
        }).state('users', {
            url: '/users',
            views: {
                "mainView": {
                    templateUrl: 'ngHarvest/Dashboard/Dashboard.html',
                    controller: 'DashboardController'
                },
                "nestedView@users": {
                    templateUrl: 'ngHarvest/Users/Users.html',
                    controller: 'UsersController'
                }
            }
        }).state('adduser', {
            url: '/adduser',
            views: {
                "mainView": {
                    templateUrl: 'ngHarvest/Dashboard/Dashboard.html',
                    controller: 'DashboardController'
                },
                "nestedView@adduser": {
                    templateUrl: 'ngHarvest/Users/AddUser.html',
                    controller: 'AddUserController'
                }
            }
        }).state('products', {
            url: '/products',
            views: {
                "mainView": {
                    templateUrl: 'ngHarvest/Dashboard/Dashboard.html',
                    controller: 'DashboardController'
                },
                "nestedView@products": {
                    templateUrl: 'ngHarvest/Products/product.html',
                    controller: 'ProductsController'
                }
            }
        }).state('purchase', {
            url: '/purchase',
            views: {
                "mainView": {
                    templateUrl: 'ngHarvest/Dashboard/Dashboard.html',
                    controller: 'DashboardController'
                },
                "nestedView@purchase": {
                    templateUrl: 'ngHarvest/Purchase/Purchase.html',
                    controller: 'PurchaseController'
                }
            }
        })
    $urlRouterProvider.otherwise('/');
}
configFunction.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider'];

AdminApp.config(configFunction);
AdminApp.run(['$rootScope', '$location', '$state', 'LoginService', function ($rootScope, $location, $state, LoginService) {

    if ($location.$$path == '/') {
        $location.$$path = '/login';
        $location.$$absUrl = $location.$$absUrl;
    }else if ($location.$$path == '/login') {
       // $state.transitionTo('login');
    }
    console.clear();
    console.log('running');
    if (!LoginService.isAuthenticated()) {
        $state.transitionTo('login');
    }
}])
AdminApp.factory('LoginService', function () {
    var admin = 'admin';
    var pass = 'password';
    var isAuthenticated = false;

    return {
        login: function (username, password) {
            isAuthenticated = username === admin && password === pass;
            return isAuthenticated;
        },
        isAuthenticated: function () {
            return isAuthenticated;
        }
    };

});