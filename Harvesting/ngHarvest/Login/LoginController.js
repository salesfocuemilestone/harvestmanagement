﻿AdminApp.controller('LoginController', ['$scope', '$rootScope', '$stateParams', '$state', 'LoginService','$location', 
    function ($scope, $rootScope, $stateParams, $state, LoginService, $location) {
        $scope.formSubmit = function () {
            if (LoginService.login($scope.username, $scope.password)) {
                $rootScope.userName = $scope.username;
                $scope.error = '';
                $scope.username = '';
                $scope.password = '';
                $state.transitionTo('home');
            } else {
                $scope.error = "Incorrect username/password !";
            }
        };
    }
]);