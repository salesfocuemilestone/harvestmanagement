﻿AdminApp.controller('DashboardController', ['$scope', '$state', '$location', 'LoginService',
    function ($scope, $state, $location, LoginService) {
        $scope.logout = function () {
            $scope.IsLogout = true;
            $state.go('login');
        }
        $scope.gohome = function () {       
            $state.go('home');            
        }
        $scope.customer = function () {
            $state.go('customer');
        }
        $scope.goUsers = function () {
            $state.go('users');
        }
        $scope.goProducts = function () {
            $state.go('products');
        }
        $scope.goPurchase = function () {
            $state.go('purchase');
        }
        if ($location.$$path == '/customer') {
            var myEl = angular.element(document.querySelector('#lnkMasters'));
            myEl.addClass('dropdownActive');
        } else if ($location.$$path == '/users') {
            var myEl = angular.element(document.querySelector('#lnkUsers'));
            myEl.addClass('dropdownActive');
        }
    }
]);